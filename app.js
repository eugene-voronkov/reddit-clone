var express = require('express')
  , passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy
  , mongodb = require('mongodb')
  , mongoose = require('mongoose')
  , bcrypt = require('bcrypt')
  , SALT_WORK_FACTOR = 10;


var async = require('async');
var q = require('q');  
var expressValidator = require('express-validator');

var jwt = require('jwt-simple');
var secret = 'xxx';



mongoose.connect('localhost', 'test');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
  console.log('Connected to DB');
});


var subSchema = mongoose.Schema({
  name: { type: String, required: true, unique: true },
  admin: { type: String, required: true }
  //mods
  //banned
  //allowed
  //private
});

var postSchema = mongoose.Schema({
  username: { type: String, required: true, unique: false },
  link: { type: String, required: false },
  comment: { type: String, required: false },
  time: { type: Number, required: false },
  upvotes: { type: Number, required: false },
  downvotes: { type: Number, required: false },
  netvotes: { type: Number, required: false },
  title: { type: String, required: true },
  sub: { type: mongoose.Schema.ObjectId, ref: 'Sub', required: false }
});

var commentSchema = mongoose.Schema({
  username: { type: String, required: true, unique: false },
  comment: { type: String, required: false },
  time: { type: Number, required: false },
  upvotes: { type: Number, required: false },
  downvotes: { type: Number, required: false },
  netvotes: { type: Number, required: false },
  parent: { type: mongoose.Schema.ObjectId, ref: 'Comment' },
  root: { type: mongoose.Schema.ObjectId, ref: 'Post' }
});


var Sub = mongoose.model('Sub', subSchema);
var Comment = mongoose.model('Comment', commentSchema);
var Post = mongoose.model('Post', postSchema);




var voteSchema = mongoose.Schema({
  contentID: {type: String},
  vote: {type: Number}
})

// User Schema
var userSchema = mongoose.Schema({
  username: { type: String, required: true, unique: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true},
  postVotes: [voteSchema],
  commentVotes: [voteSchema]
});


var postVoteSchema = mongoose.Schema({
  postID: {type: mongoose.Schema.ObjectId, ref: 'Post', index: true},
  userID: {type: mongoose.Schema.ObjectId, ref: 'User', index: true},
  vote: {type: Number},
  time: {type: Date, default: Date.now}
})

var commentVoteSchema = mongoose.Schema({
  postID: {type: mongoose.Schema.ObjectId, ref: 'Post', index: true},
  userID: {type: mongoose.Schema.ObjectId, ref: 'User', index: true},
  commentID: {type: mongoose.Schema.ObjectId, ref: 'Comment'},
  vote: {type: Number},
  time: {type: Date, default: Date.now}
})

var CommentVote = mongoose.model('CommentVote', commentVoteSchema);
var PostVote = mongoose.model('PostVote', postVoteSchema);

// Bcrypt middleware
userSchema.pre('save', function(next) {
  var user = this;

	if(!user.isModified('password')) return next();

	bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
		if(err) return next(err);

		bcrypt.hash(user.password, salt, function(err, hash) {
			if(err) return next(err);
			user.password = hash;
			next();
		});
	});
});


// Password verification
userSchema.methods.comparePassword = function(candidatePassword, cb) {
	bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
		if(err) return cb(err);
		cb(null, isMatch);
	});
};

// Seed a user
var User = mongoose.model('User', userSchema);
var user = new User({ username: 'bob', email: 'bob@example.com', password: 'secret' });
user.save(function(err) {
  if(err) {
    console.log(err);
  } else {
    console.log('user: ' + user.username + " saved.");
  }
});

new User({ username: 'testuser2', email: 'testuser2@example.com', password: 'secret' }).save();
new User({ username: 'testuser3', email: 'testuser2@example.com', password: 'secret' }).save();
// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.
passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function (err, user) {
    done(err, user);
  });
});


// Use the LocalStrategy within Passport.
//   Strategies in passport require a `verify` function, which accept
//   credentials (in this case, a username and password), and invoke a callback
//   with a user object.  In the real world, this would query a database;
//   however, in this example we are using a baked-in set of users.
passport.use(new LocalStrategy(function(username, password, done) {
  User.findOne({ username: username }, function(err, user) {
    if (err) { return done(err); }
    if (!user) { return done(null, false, { message: 'Unknown user ' + username }); }
    user.comparePassword(password, function(err, isMatch) {
      if (err) return done(err);
      if(isMatch) {
        return done(null, user);
      } else {
        return done(null, false, { message: 'Invalid password' });
      }
    });
  });
}));


var app = express();

// configure Express
app.configure(function() {
  //app.set('views', __dirname + '/views');
  //app.set('view engine', 'ejs');
  //app.engine('ejs', require('ejs-locals'));
  app.use(express.logger());
  app.use(express.cookieParser());
  app.use(express.bodyParser());

  app.use(expressValidator());

  app.use(express.methodOverride());
  app.use(express.session({ secret: 'keyboard cat' }));
  // Initialize Passport!  Also use passport.session() middleware, to support
  // persistent login sessions (recommended).
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(app.router);
  //app.use(express.static(__dirname + '/../../public'));
  app.use(express.static(__dirname + '/static'));
  //app.use(express.static('/home/gene/passport-local/examples/express3-mongoose/static'));

});


app.listen(3000, function() {
  console.log('Express server listening on port 3000');
});


function ensureAuthenticatedAPI(req, res, next) {
  var token = req.param('token');
  try {
    var userID = jwt.decode(token,secret);
  } catch(exception) {
    return res.status(401).send('401 unauthorized');
  }

  User.findById(userID, function(err, user) {
    if(user) {
      req.user = user;
      return next();
    }
    else {
      res.status(401).send('401 unauthorized');
    }
  });

}


function ensureAuthenticatedHeader(req, res, next) {
  var token = req.get('token');
  try {
    var userID = jwt.decode(token,secret);
  } catch(exception) {
    return res.status(401).send('401 unauthorized');
  }

  User.findById(userID, function(err, user) {
    if(user) {
      req.user = user;
      return next();
    }
    else {
      res.status(401).send('401 unauthorized');
    }
  });  
}


app.post('/api/login', function(req, res, next) {
  var username = req.body.username;
  var password = req.body.password;
  User.findOne({ username: username }, function(err, user) {
    console.log('user: ' + user);
    if(user) {
      user.comparePassword(password, function(err, isMatch) {
        console.log('isMatch: ' + isMatch);
        if(isMatch) {
          res.json({'token':jwt.encode(user._id,secret)});
        }
        else {
          res.status(401).json({});
        }
      });      
    }
    else {
      res.status(401).json({});
    }
  });
});


app.get('/', function(req, res) {
  res.redirect('/app/#/top');
})


app.get('/post', function(req, res) {
  Post.find({}, function(err,post) {
    res.json(post);
  });
})


app.get('/post/top', function(req, res) {
  console.log('token:' + req.get('token'));
  //console.log(req.query.page);
  var page=0;
  if(req.query.page == undefined ) {
    req.query.page = 0;
  }
  if(req.query.page > 0) {
    page = req.query.page-1;
  }
  
  Post.find({ $query: {}, $orderby: {netvotes: -1} }, {}, {limit:25, skip:page*25}, function(err,post) {
    console.log(post);
    res.json(post);
  });
})


app.get('/post/:postid', function(req, res) {
  Post.findOne({_id: req.params.postid}, function(err,post) {
    res.json(post);
  });
});


app.put('/post/:postid', ensureAuthenticatedHeader, function(req, res) {
  req.checkBody('comment', 'Comment must be 1-15000 characters long.').isLength(1,15000);
  var errors = req.validationErrors();
  if(errors) {
    console.log(errors);
    return res.status(400).json(errors);
  }

  req.sanitize("comment").escape();

  async.waterfall([
    function(callback) {
      Post.findOne({_id: req.params.postid}, function(err,post) {
        if(err || !post) {
          return callback(true);
        }
        if(req.user.username != post.username) {
          return res.status(401).send('401 unauthorized');
        }

        post.comment = req.body.comment;
        post.save();
        res.json(post);
      })
    }
    ],function() {
      res.status(400).json();
    })

});

app.delete('/post/:postid', ensureAuthenticatedHeader, function(req, res) {
  async.waterfall([
    function(callback) {
      Post.findOne({_id: req.params.postid}, function(err,post) {
        if(err || !post) {
          return callback(true);
        }

        if(req.user.username != post.username) {
          //return callback(true);
          return res.status(401).send('401 unauthorized');
        }
        else {
          post.remove(function() {
            res.json();
          });
        }
      })
    }
    ], function() {
        res.status(400).json();
  })
  Post.findOne({_id: req.params.postid}, function(err,post) {

  })
})


app.post('/api/post/votes', ensureAuthenticatedHeader, function(req, res) {
  PostVote.find({'postID': { $in: req.body.post_id_list }, 'userID':req.user._id}, function(err, votes) {
    if(err || !votes) {
      return res.status(400).json();
    }
    console.log(votes);
    return res.json(votes);
  })
  //return res.json(req.body.post_id_list);
});


app.get('/api/post/:postid/comment/votes', ensureAuthenticatedHeader, function(req, res) {
  CommentVote.find({'postID': req.params.postid, 'userID': req.user._id}, function(err, votes) {
    if(err || !votes) {
      return res.status(400).json();
    }
    return res.json(votes);
  });
});


app.post('/api/comment/votes', ensureAuthenticatedHeader, function(req, res) {
  CommentVote.find({'commentID': { $in: req.body.post_id_list }, 'userID':req.user._id}, function(err, votes) {
    if(err || !votes) {
      return res.status(400).json();
    }
    console.log(votes);
    return res.json(votes);
  })
  //return res.json(req.body.post_id_list);
});


app.put('/api/post/:postid/comment/:commentid/vote', ensureAuthenticatedHeader, function(req, res) {

  async.waterfall([
  function(callback) {
    Comment.findOne({_id: req.params.commentid}, function(err,comment) {
      if(err || !comment) {
        return res.status(400).json();
      }
      callback(null,comment);
    });  
  },

    function(comment, callback) {
      CommentVote.findOne({commentID: comment._id, userID: req.user._id}, function(err, votes) {

        var new_vote;
        if(req.body.vote <= -1) {
          new_vote = -1;
        }
        else if(req.body.vote >= 1) {
          new_vote = 1;
        }
        else {
          new_vote = 0;
        }
        if(votes && new_vote == votes.vote) {
          return res.status(400).json();
        }


        if(!votes) {
          var vote = new CommentVote({postID: req.params.postid, commentID: comment._id, userID: req.user._id, vote: new_vote}).save();
        }
        else if(votes && new_vote != votes.vote) {
          votes.vote = new_vote;
          votes.save();

          if(new_vote == -1) {
            comment.upvotes -= 1;
          } else if(new_vote == 1) {
            comment.downvotes  -= 1;
          }
        }

        if(new_vote == -1) {
          comment.downvotes += 1;
        } else if(new_vote == 1) {
          comment.upvotes += 1;
        }
        comment.netvotes = comment.upvotes - comment.downvotes;
        comment.save();

        console.log(votes);
        //return res.json(vote);
        return res.json({postID: req.params.postid, commentID: comment._id, userID: req.user._id, vote: new_vote});
      })
    }  
  ],function() {
      res.status(400).json({});
  })

});


app.delete('/api/post/:postid/comment/:commentid/vote', ensureAuthenticatedHeader, function(req, res) {

  CommentVote.findOne({postID: req.params.postid, userID: req.user._id, commentID: req.params.commentid}, function(err, votes) {
    if(err || !votes) {
      return res.status(400).json();
    }

    votes.remove(function() {
      return res.json();
    })
  });

});


app.delete('/api/post/:postid/vote', ensureAuthenticatedHeader, function(req, res) {
  PostVote.findOne({postID: req.params.postid, userID: req.user._id}, function(err, votes) {
    if(err || !votes) {
      return res.status(400).json();
    }

    votes.remove(function() {
      return res.json();
    })
  });
});


app.put('/api/post/:postid/vote', ensureAuthenticatedHeader, function(req, res) {

  async.waterfall([
    function(callback) {
      Post.findOne({_id: req.params.postid}, function(err,post) {
        if(err || !post) {
          return res.status(400).json();
        }
        callback(null, post);
      });      
    },
    function(post, callback) {
      PostVote.findOne({postID: post._id, userID: req.user._id}, function(err, votes) {

        var new_vote;
        if(req.body.vote <= -1) {
          new_vote = -1;
        }
        else if(req.body.vote >= 1) {
          new_vote = 1;
        }
        else {
          new_vote = 0;
        }
        if(votes && new_vote == votes.vote) {
          return res.status(400).json();
        }



        if(!votes) {
          votes = new PostVote({postID: post._id, userID: req.user._id, vote: new_vote}).save();
        }
        else if(votes && new_vote != votes.vote) {
          votes.vote = new_vote;
          votes.save();

          if(new_vote == -1) {
            post.upvotes -= 1;
          } else if(new_vote == 1) {
            post.downvotes  -= 1;
          }
        }

        if(new_vote == -1) {
          post.downvotes += 1;
        } else if(new_vote == 1) {
          post.upvotes += 1;
        }
        post.netvotes = post.upvotes - post.downvotes;
        post.save();

        console.log(votes);
        //return res.json(votes);
        return res.json({postID: post._id, userID: req.user._id, vote: new_vote});
      })
    }
    ],function() {

    })

});


app.post('/api/post', ensureAuthenticatedHeader, function(req, res) {
  req.checkBody('comment', 'Comment must be 1-15000 characters long.').isLength(1,15000);
  req.checkBody('title', 'Title must be 1-300 characters long.').isLength(1,300)
  var errors = req.validationErrors();
  if(errors) {
    console.log(errors);
    return res.status(400).json(errors);
  }

  req.sanitize('comment').escape();
  req.sanitize('title').escape();

  var post = new Post({ username: req.user.username, comment: req.body.comment, title: req.body.title, upvotes: 0, downvotes: 0 });
  post.save();

  res.json(post);

});


app.post('/api/register', function(req, res) {
  req.checkBody('username', 'Username is required.').notEmpty();
  req.checkBody('username', 'Username must be between 3-12 characters long.').isLength(3,12);
  req.checkBody('username', 'Username must be alphanumeric.').isAlphanumeric();

  req.checkBody('email', 'Must be a valid email address.').isEmail();

  req.checkBody('password', 'Password is required.').notEmpty();
  req.checkBody('password', 'Password must be between 6-24 characters long.').isLength(6,24);

  var errors = req.validationErrors();
  if(errors) {
    console.log(errors);
    return res.status(400).json(errors);
  }

  var user = new User({ username: req.body.username, email: req.body.email, password: req.body.password });
  user.save(function(err) {
    if(err) {
      console.log(err);
      res.status(401).json();
    } else {
      res.json({'token': jwt.encode(user._id, secret)})
    }
  });  
})


/*
var req = {
  param: {
    postid: 'foo',
  },
  body: {
    comment_parent: '',
    username: 'bob',
    comment: 'test comment',
  }
}
*/

//curl --data "username=bob&comment=test&commentparent=''" http://localhost:3000/api/post/543ca5aca0434d7b0b99503d/comment
app.post('/api/post/:postid/comment', ensureAuthenticatedHeader, function(req, res) {
  req.checkBody('comment', 'Comment must be 1-15000 characters long.').isLength(1,15000);
  var errors = req.validationErrors();
  if(errors) {
    console.log(errors);
    return res.status(400).json(errors);
  }

  req.sanitize('comment').escape();

  async.waterfall([
    //verify root post exists
    function(callback) {
      console.log('callback 1');
      Post.findById(req.params.postid, function(err, post) {
        console.log(req.params.postid);
        console.log(err);
        console.log(post);
        if(err || !post)
          return callback(true);

        callback(null);
      })
    },
    //verify parent comment exists
    function(callback) {
      console.log('callback 2');
      console.log('parent comment:' + req.body.comment_parent);
      if(req.body.comment_parent == '') {
        console.log('comment_parent check');
        return callback(null);
      }
      Comment.findById(req.body.comment_parent, function(err, comment) {
        if(err || !comment) {
          return callback(true);
        }
        else {
          return callback(null);
        }
      });
    },
    //save comment
    function(callback) {
      console.log('callback 3');
      var comment;
      if(req.body.comment_parent == '') {
        comment = new Comment({username: req.user.username, comment:req.body.comment, root: req.params.postid, netvotes:0, upvotes: 0, downvotes:0});        
      }
      else {
        comment = new Comment({username: req.user.username, comment:req.body.comment, parent:req.body.comment_parent, root: req.params.postid, netvotes:0, upvotes: 0, downvotes:0});        
      }
      comment.save();

      return res.json(comment);  
    }
  ],
  function() {
    res.status(400).json();
    console.log('exception');
    console.log('error')

  });

});



app.get('/api/post/:postid/comment', function(req, res) {

  Comment.find({root:req.params.postid}, function(err, comment) {
    if(err || !comment) {
      return res.status(400).json();
    }
    res.json(comment);
  });

});


app.put('/api/post/:postid/comment/:commentid', ensureAuthenticatedHeader, function(req, res) {
  req.checkBody('comment', 'Comment must be 1-15000 characters long.').isLength(1,15000);
  var errors = req.validationErrors();
  if(errors) {
    console.log(errors);
    return res.status(400).json(errors);
  }
  req.sanitize('comment').escape();

  async.waterfall([
  function(callback) {
    Comment.findById(req.params.commentid, function(err, comment) {
      if(err || !comment) {
        return callback(true)
      }
      if(req.user.username != comment.username) {
        return res.status(401).json();
      }

      comment.comment = req.body.comment;
      comment.save();
      return res.json(comment);
    })
  }
  ], function() {
      return res.status(400).json();
  })
})


app.delete('/api/post/:postid/comment/:commentid', ensureAuthenticatedHeader, function(req, res) {
  async.waterfall([
  function(callback) {
    Comment.findById(req.params.commentid, function(err, comment) {
      if(err || !comment) {
        return callback(true)
      }
      if(req.user.username != comment.username) {
        return res.status(401).json();
      }
      
      comment.username = '[deleted]'
      comment.comment = '[deleted]';
      comment.save();
      return res.json(comment);
    })
  }
  ], function() {

  })
})


app.get('/api/post/:postid/comment/:commentid', function(req, res) {

  Post.findById(req.params.postid, function(err, post) {
    if(err || !post) {
      return res.status(400).json();
    }
    Comment.findById(req.params.commentid, function(err, comment) {
      if(err || !comment) {
        return res.status(400).json();
      }
      return res.json(comment);
    });
  });
});


//create a sub
app.post('/api/sub', ensureAuthenticatedHeader, function(req, res) {

  data = { name: req.body.name, admin: req.user.username }
  new Sub(data).save(function(err) {
    if(err) {
      return res.status(400).json();
    }
    return res.json(data)
  });
})


app.post('/api/sub/:sub_name/post', ensureAuthenticatedHeader, function(req, res) {
  req.checkBody('comment', 'Comment must be 1-15000 characters long.').isLength(1,15000);
  req.checkBody('title', 'Title must be 1-300 characters long.').isLength(1,300)
  var errors = req.validationErrors();
  if(errors) {
    console.log(errors);
    return res.status(400).json(errors);
  }

  Sub.findOne({name: req.params.sub_name}, function(err, sub) {
    if(err) {
      return res.status(400).json();
    }
  
    req.sanitize('comment').escape();
    req.sanitize('title').escape();
    var post = new Post({ username: req.user.username, comment: req.body.comment, title: req.body.title, upvotes: 0, downvotes: 0, sub: sub });
    post.save(function(err) {
      console.log(err);
    });

    res.json(post);
  })

});


app.get('/api/sub/:sub_name/post', function(req, res) {
  Sub.findOne({name: req.params.sub_name}, function(err, sub) {
    if(err) {
      return res.status(400).json();
    }

    Post.find({sub: sub}, function(err, posts) {
      console.log(posts);
      return res.json(posts);
    })
  })
})

/*
app.get('/post/:postid/comment/:commentid', function(req, res) {

});

app.put('/post/:postid/comment/:commentid', function(req, res) {

});

app.delete('/post/:postid/comment/:commentid', function(req, res) {

});

app.put('/post/:postid/comment/:commentid/vote', function(req, res) {

});

app.post('/post/:postid/comment', function(req, res) {

});

app.get('/post/:postid/comment', function(req, res) {

});



app.delete('/post/:postid', function(req, res) {

});

app.get('/post/:postid/vote', function(req, res) {

});
*/