'use strict';

/* Filters */

angular.module('myApp.filters', []).
  filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    }
  }])

  .filter('markdown', function ($sce, $sanitize) {
    var converter = new Showdown.converter();
    return function (value) {
		var html = $sanitize(converter.makeHtml(value || ''));
        return $sce.trustAsHtml(html);
    };
  });
