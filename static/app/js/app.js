'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ngSanitize',
  'ngRoute',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/top/:page?', {templateUrl: 'partials/top.html', controller: 'TopCtrl'});  
  $routeProvider.when('/', {templateUrl: 'partials/frontpage.html', controller: 'FrontPageCtrl'});
  //$routeProvider.when('/post/:postID', {templateUrl: 'partials/post.html', controller: 'PostCtrl'});
  $routeProvider.when('/post/:postID', {templateUrl: 'partials/post.html'});
  $routeProvider.when('/submit', {templateUrl: 'partials/submit.html', controller: 'SubmitCtrl'});
  //$routeProvider.when('/login', {templateUrl: 'partials/login.html', controller: 'LoginCtrl'}); 
  $routeProvider.when('/newsub', {templateUrl: 'partials/newsub.html', controller: 'NewSubCtrl'});
  $routeProvider.when('/sub/:subName', {templateUrl: 'partials/subfrontpage.html'});
  $routeProvider.when('/sub/:subName/submit', {templateUrl: 'partials/submit.html', controller: 'SubmitCtrl'});  
  $routeProvider.otherwise({redirectTo: '/'});
}]);
