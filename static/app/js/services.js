'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('myApp.services', [])
  .factory('tokenFactory', function($window, $http) {
    var struct = {
      'token': null,
      'username': null
    } 

    var token = null;
    var username = null;
    delete $http.defaults.headers.common['token']

    if($window.sessionStorage.token != null && $window.sessionStorage.username != null) {
      struct.token = $window.sessionStorage.token;
      struct.username = $window.sessionStorage.username;
      $http.defaults.headers.common['token'] = struct.token;
    }

    var setLoginSession = function(new_token, new_username) {
      $window.sessionStorage.setItem('token', new_token);
      $window.sessionStorage.setItem('username', new_username);
      $http.defaults.headers.common['token'] = new_token;

      struct.token = new_token;
      struct.username = new_username;
    }

    var getToken = function() {
      return $window.sessionStorage.token;
    }
    var getUsername = function() {
      return $window.sessionStorage.username;
    }

    return {
      'token': $window.sessionStorage.token,
      'username': $window.sessionStorage.username,
      'setLoginSession': setLoginSession,
      'getToken': getToken,
      'getUsername': getUsername,
      'tokenMap': struct
    }
  })


  .factory('commentFactory', function() {
    var commentarray2map = function(list) {
      var i;
      var map = {};
      for(i=0; i<list.length; ++i) {
        list[i].children = [];
        map[list[i]._id] = list[i];
      }
      return map;
    }

    var dfs = function(tree) {
      tree.sort(function(a,b){return b.netvotes - a.netvotes});
      console.log('tree');
      console.log(tree);
      var i=0;
      for(i=0; i<tree.length; ++i) {
        dfs(tree[i].children);
      }
      return tree;
    }

    var createTree = function(list) {
      var map = commentarray2map(list);
      var i;
      for(i=0; i<list.length; ++i) {
        var parent_id = list[i].parent;
        if(parent_id != '' && parent_id != undefined) {
          map[parent_id].children.push(list[i]);
        }
      }
      var tree_root = []
      for(i=0; i<list.length; ++i) {
        var parent_id = list[i].parent;
        if(parent_id == '' || parent_id == undefined) {
          tree_root.push(list[i]);
        }
      }
      dfs(tree_root);

      return tree_root;

    }

  	return {
  		'createCommentTree': createTree
  	}
  })


  .factory('commentVoteFactory', function($http, tokenFactory, commentFactory) {
    var votes = {
      'map': {},
      'history': [],
      'updateTime': 0,
      'commentTree': {}
    };

    var comments = {
      'list': [],
      'tree': {}
    }

    var getVotes = function(postList) {
      var i=0;
      var id_list = [];
      for(i=0; i<postList.length; ++i) {
        id_list.push(postList[i]._id);
      }

      $http.post('../api/comment/votes', {post_id_list: id_list}).success(function(data) {          
        votes.history = data;
        var voteMap = {};
        var j=0;

        for(j=0; j < votes.history.length; ++j) {
          voteMap[votes.history[j].commentID] = votes.history[j];
        }

        votes.map = voteMap;
        votes.updateTime = Date.now();

      }).error(function() {
        votes.updateTime = Date.now();
      })

    }

    var getComments = function(postID) {
      $http.get('../api/post/' + postID + '/comment').success(function(data,status) {
        console.log('getPosts comments');
        //$scope.comments = data;
        comments.list = data;
        comments.tree = commentFactory.createCommentTree(comments.list);
        console.log(comments.tree);
        console.log(data);
        if(tokenFactory.getToken() != null) {
          getVotes(data);
        }
      })
    }

    var upvote = function(submission) {
      var url = '../api/post/' + submission.root + '/comment/' + submission._id + '/vote';
      var data = {'vote': 1};
      $http.put(url, data).success(function(data, status) {
        submission.upvotes += 1;
        submission.netvotes = submission.upvotes - submission.downvotes;        
        votes.map[data.commentID] = data;
        votes.updateTime = Date.now();
      });

    }

    var downvote = function(submission) {
      var url = '../api/post/' + submission.root + '/comment/' + submission._id + '/vote';
      var data = {'vote': -1};
      $http.put(url, data).success(function(data, status) {
        submission.downvotes += 1;
        submission.netvotes = submission.upvotes - submission.downvotes;
        console.log(data);
        votes.map[data.commentID] = data;
        votes.updateTime = Date.now();
      });

    }

    var deletevote = function(submission) {
      var url = '../api/post/' + submission.root + '/comment/' + submission._id + '/vote';
      $http.delete(url).success(function(data) {
        delete votes.map[submission._id]
        votes.updateTime = Date.now();
      })
    }

    return {
      'votes': votes,
      'comments': comments,
      'getVotes': getVotes,
      'getComments': getComments,
      'upvote': upvote,
      'downvote': downvote,
      'deletevote': deletevote
    }
  })


  .factory('postVoteFactory', function($http, tokenFactory) {
    var votes = {
      'map': {},
      'history': [],
      'updateTime': 0
    };

    var posts = {
      'list': []
    }

    var getVotes = function(postList) {
      var i=0;
      var id_list = [];
      for(i=0; i<postList.length; ++i) {
        id_list.push(postList[i]._id);
      }

      $http.post('../api/post/votes', {post_id_list: id_list}).success(function(data) {        
        votes.history = data;
        var voteMap = {};
        var j=0;

        for(j=0; j < votes.history.length; ++j) {
          voteMap[votes.history[j].postID] = votes.history[j];
        }
        votes.map = voteMap;
        votes.updateTime = Date.now();

      }).error(function() {
        console.log('getVotes error');
        votes.updateTime = Date.now();
      })
    }

    var getPosts = function(page_number, sub) {
      var url;
      if(!sub) {
        url = '../post/top?page=' + page_number
      } else {
        url = '../api/sub/' + sub + '/post'
      }
      $http.get(url).success(function(data,status,headers,config) {
        posts.list = data;
        if(tokenFactory.getToken() != null) {
          getVotes(data);
        }
      });
    }

    return {
      'votes': votes,
      'getVotes': getVotes,
      'posts': posts,
      'getPosts': getPosts
    }
  });
  
