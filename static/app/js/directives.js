'use strict';

/* Directives */


angular.module('myApp.directives', []).
  directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }])



.directive("commenttree2", function($compile) {
    return {
        restrict: "E",
        transclude: true,
        scope: {family: '='},
        template:       
            '<ul>' + 
                '<li ng-transclude></li>' +
                '<li ng-repeat="child in family.children">' +
                    '<commenttree2 family="child"><blockquote><h8>posted by: {{family.username}}</h8><h4>{{family.comment}}</h4><a href="#">reply</a><textarea class="form-control" rows="3" ng-model="reply_message"></textarea></blockquote></commenttree2>' +
                '</li>' +
            '</ul>',
        compile: function(tElement, tAttr, transclude) {
            var contents = tElement.contents().remove();
            var compiledContents;
            return function(scope, iElement, iAttr) {
                if(!compiledContents) {
                    compiledContents = $compile(contents, transclude);
                }
                compiledContents(scope, function(clone, scope) {
                         iElement.append(clone); 
                });
            };
        }
    };
})



.directive("commentMarkdown", function () {
    var converter = new Showdown.converter();
    return {
        restrict: "AE",
        scope: {
            text: "=commentText"
        },
        link: function(scope, element,attrs) {
            //console.log(element.text());
            console.log(scope);
            console.log(scope.text);
            //console.log(element.html());
            var htmlText = converter.makeHtml(scope.text);
            element.html(htmlText);
            element.css("{background-color: black}");
            //console.log(htmlText);
            
        },
        template: "{{text}}"
    };
})



.directive('markdown', function () {
    var converter = new Showdown.converter();
    return {
        restrict: 'AE',
        link: function (scope, element, attrs) {
        	/*
        	scope.$watch(attrs.markdown, function (newVal) {
				var html = newVal ? converter.makeHtml(newVal) : '';
				element.html(html);
			});
			*/

			if (attrs.markdown) {
				scope.$watch(attrs.markdown, function (newVal) {
					var html = newVal ? converter.makeHtml(newVal) : '';
					element.html(html);
				});
			} else {
				var html = converter.makeHtml(element.text());
				element.html(html);
			}

            //var htmlText = converter.makeHtml(element.text());
            //element.html(htmlText);
        }
    };

})



.directive('comments', function () {
    return {
        restrict: "E",
        //replace: true,
        template: "<collection2 collection='comments.tree' reply='post_reply' edit='edit' delete='delete' upvote='upvote' downvote='downvote'></collection2>",
        link: function(scope, element, attrs) {
        }
    }
})



.directive('collection2', function () {
    return {
        restrict: "E",
        transclude: true,
        //replace: true,
        scope: {
            collection: '=', reply: '&', edit: '&', delete: '&', upvote: '&', downvote: '&'
        },
        template: "<div class='commentclass'><ul><member2 ng-repeat='member in collection' member='member' reply='reply()' edit='edit()' delete='delete()' upvote='upvote()' downvote='downvote()'></member2></ul></div>",

        link: function(scope, element, attrs) {

            //console.log(scope.collection);
        }
    }
})



//refactor out isolated scope to pass in services through controller.
.directive('member2', function ($compile, tokenFactory, $rootScope) {
    return {
        restrict: "E",
        //replace: true,
        
        scope: {
            member: '=', reply: '&', edit: '&', delete: '&', upvote: '&', downvote: '&'
        },

        template: '<li><blockquote><h8><vote-comment class="vote"></vote-comment> votes:  {{member.upvotes}} {{member.netvotes}} {{member.downvotes}} posted by: {{member.username}}</h8><h4>{{member.comment}}</h4><div class="user-controls"><a class="reply" href="" style="display:none">reply</a> <a class="edit logged-in login-{{member.username}}" href="" style="display:none">edit</a> <a ng-click="delete()(member)" class="delete" href="" style="display:none">delete</a></div> <div class="reply-form" style="display:none"><textarea class="form-control" rows="3" ng-model="reply_message"></textarea> <button ng-click="reply()(member, reply_message)" class="btn btn-default reply_submit">reply</button></div> <div class="edit-form" style="display:none"> <textarea class="form-control" rows="3" ng-model="member.comment"></textarea> <button ng-click="edit()(member, member.comment)" class="edit_submit_button">edit</button></div></blockquote>    </li>',
        link: function (scope, element, attrs) {

            var collectionSt = '<collection2 collection="member.children" reply="reply()" edit="edit()" delete="delete()" upvote="upvote()" downvote="downvote()"></collection2>';
            console.log(scope.member);
            if (angular.isArray(scope.member.children)) {       
              
                $compile(collectionSt)(scope, function(cloned, scope)   {
                    element.append(cloned); 
                  });
            }

            console.log(scope.member);

            var reply_link = element.find('.reply');
            var comment_box = element.find('.reply-form');
            var edit_link = element.find('.edit');
            var edit_box = element.find('.edit-form');
            var delete_link = element.find('.delete');
            var reply_submit_button = element.find('.reply_submit');
            var edit_submit_button = element.find('.edit_submit_button');
            var vote_buttons = element.find('.vote');

            if(scope.member.username == '[deleted]') {
                vote_buttons.hide();
            }

            delete_link.click(function() {
                var disableListener = $rootScope.$on('delete-comment-'+scope.member._id, function() {
                    console.log('$on delete');
                    reply_link.hide();
                    edit_link.hide();
                    delete_link.hide();
                    vote_buttons.hide();
                    disableListener();
                })
            })

            edit_link.click(function() {
                edit_box.toggle();
            })
            reply_link.click(function() {
                comment_box.toggle();

            })

            reply_submit_button.click(function() {
                console.log('reply_submit_button');
                var disableListener = $rootScope.$on('reply-comment-'+scope.member._id, function() {
                    comment_box.hide();
                    disableListener();
                })                
            })

            edit_submit_button.click(function() {
                console.log('edit button clicked');
                var disableListener = $rootScope.$on('edit-comment-'+scope.member._id, function() {
                    edit_box.hide();
                    disableListener();
                })
            });

            var user_controls = element.find('.user-controls');

            scope.tokenMap = tokenFactory.tokenMap;
            scope.$watch(function() {
                return scope.tokenMap.username;
            },
            function() {
                if(scope.tokenMap.username != null) {
                    reply_link.show();
                }          
                if (scope.tokenMap.username != null && scope.tokenMap.username == scope.post.username) {
                    
                    delete_link.show();
                    edit_link.show();
                    reply_link.show();
                }
            })

        }
    }
})



.directive('post', function($rootScope) {
    return {
        restrict: 'E',
        template: '<h4>{{post.upvotes}} {{post.downvotes}}<h8><vote></vote> {{post.title}} </h4> <p>posted by: {{post.username}}</p> <h4><div ng-bind-html="post.comment | markdown"></div></h4> <div class="user-controls"><a class="reply" href="">reply</a> <a class="reply2" href="">reply2</a> <a class="edit logged-in login-{{member.username}}" href="">edit</a> <a class="delete" ng-click="delete(post)" href="">delete</a></div> <div class="reply-form" style="display:none"><textarea class="form-control" rows="3" ng-model="reply_message"></textarea> <button ng-click="$parent.post_reply_parent(reply_message)" class="btn btn-default submit_reply">reply</button></div> <div class="edit-form" style="display:none"> <textarea class="form-control" rows="3" ng-model="postEdit.comment"></textarea> <button ng-click="editPost(postEdit)" class="submit_edit">edit</button></div>',
        link: function(scope, element, attrs) {
            var reply_link = element.find('.reply');
            var comment_box = element.find('.reply-form');

            var edit_link = element.find('.edit');
            var edit_box = element.find('.edit-form');

            var delete_link = element.find('.delete');
            var reply2_link = element.find('.reply2');

            var submit_reply = element.find('.submit_reply');
            var submit_edit = element.find('.submit_edit');

            submit_reply.click(function() {
                console.log('submit_reply');
                //comment_box.hide();
                var disableListener = $rootScope.$on('reply-post', function() {
                    comment_box.hide();
                    disableListener();
                });
            })

            submit_edit.click(function() {
                var disableListener = $rootScope.$on('edit-post', function() {
                    edit_box.hide();
                    disableListener();
                })
            })
            edit_link.click(function() {
                edit_box.toggle();
            })
            reply_link.click(function() {
                comment_box.toggle();
                //reply_link.toggle();
            })

            edit_link.hide();
            reply_link.hide();
            delete_link.hide();
            reply2_link.hide();
            
            scope.$watch(function() {
                return scope.tokenMap.username;
            },
            function() {
                
                if(scope.tokenMap.username != null) {
                    reply_link.show();
                }
                
                if (scope.tokenMap.username != null && scope.tokenMap.username == scope.post.username) {
                    delete_link.show();
                    edit_link.show();
                    reply_link.show();
                }
            })
            
            scope.$watch(function() {
                return scope.post;
            },
            function() {
                if(scope.tokenMap.username != null) {
                    reply_link.show();
                }
                
                if (scope.tokenMap.username != null && scope.tokenMap.username == scope.post.username) {
                    delete_link.show();
                    edit_link.show();
                    reply_link.show();
                }                
            })
            

        }
    }
})



.directive('watchlogin', function(postVoteFactory) {
    return {
        restrict: 'E',
        transclude: true,
        template: '<div><div ng-transclude></div></div>',
        link: function(scope, element, attrs) {
            console.log('postVoteFactory');
            console.log(postVoteFactory);
            scope.$watch(function() {
                return scope.logged_in;
                //return scope.logged_in;
            },
            function(newVal, oldVal) {
                if(scope.logged_in && postVoteFactory.posts.list.length != 0 && newVal !== oldVal) {
                    postVoteFactory.getVotes(postVoteFactory.posts.list);
                }
                console.log('logged in watched');
                console.log(newVal);
                console.log(oldVal);
                console.log(scope.logged_in);
            })
        }

    }
})



//easier to pass services into directive since directive uses isolated scope.
//refactor tree layout code to not use isolate scope
.directive('voteComment', function(tokenFactory, commentVoteFactory) {
    return {
        restrict: "E",
        template: '<button type="button" class="btn btn-default btn-xs upvote"><span class="glyphicon glyphicon-arrow-up"></span></button><button type="button" class="btn btn-default btn-xs downvote"><span class="glyphicon glyphicon-arrow-down"></span></button>',
        link: function(scope, element, attrs) {
            console.log('logged_in');
            console.log(scope.logged_in);
            console.log('tokenFactory');
            console.log(tokenFactory);

            scope.upvote = commentVoteFactory.upvote;
            scope.downvote = commentVoteFactory.downvote;
            scope.deletevote = commentVoteFactory.deletevote;
            
            scope.votes = commentVoteFactory.votes;
            scope.post = scope.member;
            
            var upvote = element.find('.upvote');
            var downvote = element.find('.downvote');

            //
            var vote = null;
            scope.$watch(function() {
                return scope.votes.updateTime;
            },
            function() {
                console.log('$watch observed voteUpdateTime');
                console.log(scope.post);
                console.log(scope.votes);
                //console.log(scope.posts);
                
                vote = scope.votes.map[scope.post._id];
                console.log('post._id');
                console.log(scope.post._id);
                console.log('map')
                console.log(scope.votes.map);
                console.log('vote');
                console.log(vote);
                upvote.css('color', 'black');
                downvote.css('color', 'black');
                
                if(vote != null && vote != undefined) {
                    if(vote.vote == 1) {
                        //upvote.hide();
                        upvote.css('color', 'red');
                    }
                    else if(vote.vote == -1) {
                        //downvote.hide();
                        downvote.css('color', 'red');
                    }
                }
                
            });
            

            upvote.click(function() {
                console.log('upvote');
                console.log(vote);
                //console.log(scope.voteMap[scope.post._id])
                //commentVoteFactory.upvote(scope.post);
                if(vote==null) {
                    console.log('is null');
                    scope.upvote(scope.post);
                }
                else if(vote.vote == -1) {
                    console.log(scope.post);
                    scope.upvote(scope.post);
                }
                else {
                    scope.deletevote(scope.post);
                }
            });

            downvote.click(function() {
                console.log('downvote');
                console.log(vote);
                console.log('scope');
                console.log(scope);
                //console.log(scope.voteMap[scope.post._id]);
                if(vote==null) {
                    scope.downvote(scope.post);
                }
                else if(vote.vote == 1) {
                    scope.downvote(scope.post);
                }
                else {
                    scope.deletevote(scope.post);
                }
            })
        }
    }
})



.directive('vote', function () {
    return {
        restrict: "E",
        /*
        transclude: true,
        scope: {
            post: '=', votehistory: '='
        },
        */
        template: '<button type="button" class="btn btn-default btn-xs upvote"><span class="glyphicon glyphicon-arrow-up"></span></button><button type="button" class="btn btn-default btn-xs downvote"><span class="glyphicon glyphicon-arrow-down"></span></button>',
        link: function(scope, element, attrs) {
            console.log('logged_in');
            console.log(scope.logged_in);

            var upvote = element.find('.upvote');
            var downvote = element.find('.downvote');

            var vote = null;
            scope.$watch(function() {
                return scope.votes.updateTime;
            },
            function() {
                //console.log('$watch observed voteUpdateTime');
                //console.log(scope.post);
                //console.log(scope.posts);
                
                vote = scope.votes.map[scope.post._id];
                upvote.css('color', 'black');
                downvote.css('color', 'black');
                
                if(vote != null && vote != undefined) {
                    if(vote.vote == 1) {
                        //upvote.hide();
                        upvote.css('color', 'red');
                    }
                    else if(vote.vote == -1) {
                        //downvote.hide();
                        downvote.css('color', 'red');
                    }
                    /*
                    else {
                        upvote.css('color', 'black');
                        downvote.css('color', 'black');
                    }
                    */
                }
                
            });


            upvote.click(function() {
                console.log('upvote');
                console.log(vote);
                //console.log(scope.voteMap[scope.post._id])
                if(vote==null) {
                    scope.upvote(scope.post);
                }
                else if(vote.vote == -1) {
                    console.log(scope.post);
                    scope.upvote(scope.post);
                }
                else {
                    scope.deletevote(scope.post);
                }
            });

            downvote.click(function() {
                console.log('downvote');
                console.log(vote);
                //console.log(scope.voteMap[scope.post._id]);
                if(vote==null) {
                    scope.downvote(scope.post);
                }
                else if(vote.vote == 1) {
                    scope.downvote(scope.post);
                }
                else {
                    scope.deletevote(scope.post);
                }
            })


        }
    }

});
