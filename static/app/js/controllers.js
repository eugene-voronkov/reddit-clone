'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
  .controller('FrontPageCtrl', ['$scope', '$http','tokenFactory',function($scope, $http, tokenFactory) {

		$http.get('../post').success(function(data,status,headers,config) {
			$scope.foo = data;
			$scope.posts = data;
		}).
  	error(function(data,status,headers,config) {

		});

  	$scope.upvote = function(submission) {
  		var url = '../api/post/' + submission._id + '/vote?token=' + tokenFactory.token;
  		var data = {'vote': 1};
  		$http.put(url, data).success(function(data, status) {
  			submission.upvotes += 1;
  		});

  	}

  	$scope.downvote = function(submission) {
  		var url = '../api/post/' + submission._id + '/vote?token=' + tokenFactory.token;
  		var data = {'vote': -1};
  		$http.put(url, data).success(function(data, status) {
  			submission.downvotes += 1;
  		});

  	}
  }])


  .controller('TopSubCtrl', ['$scope', '$http', '$routeParams','tokenFactory', 'postVoteFactory', '$q',function($scope, $http, $routeParams, tokenFactory, postVoteFactory, $q) {
    $scope.posts = [];

    var fetchData = function() {
      $http.get('../api/sub/' + $routeParams.subName).success(function(data,status,headers,config) {
        $scope.posts = data;

      });
    }    

    //fetchData();

    postVoteFactory.getPosts($routeParams.page, $routeParams.subName);;
    $scope.posts = postVoteFactory.posts;
    $scope.votes = postVoteFactory.votes;

  }])


  .controller('TopCtrl', ['$scope', '$http', '$routeParams','tokenFactory', 'postVoteFactory', '$q',function($scope, $http, $routeParams, tokenFactory, postVoteFactory, $q) {
    if($routeParams.page == undefined) {
      $scope.page = 1;
    }
    else {
      $scope.page = parseInt($routeParams.page);      
    }

    var fetchData = function() {
      $http.get('../post/top?page=' + $routeParams.page).success(function(data,status,headers,config) {
        $scope.posts = data;

      });
    }

    $scope.votes = postVoteFactory.votes;
    $scope.posts = postVoteFactory.posts;
    postVoteFactory.getPosts($routeParams.page, null);

    $scope.upvote = function(submission) {
      var url = '../api/post/' + submission._id + '/vote?token=' + tokenFactory.token;
      var data = {'vote': 1};
      $http.put(url, data).success(function(data, status) {
        submission.upvotes += 1;
        //$scope.voteMap[data.postID] = data;
        $scope.votes.map[data.postID] = data;
        //$scope.votesUpdateTime = Date.now()
        $scope.votes.updateTime = Date.now()
      });

    }

    $scope.downvote = function(submission) {
      var url = '../api/post/' + submission._id + '/vote?token=' + tokenFactory.token;
      var data = {'vote': -1};
      $http.put(url, data).success(function(data, status) {
        submission.downvotes += 1;
        //$scope.voteMap[data.postID] = data;
        $scope.votes.map[data.postID] = data;
        //$scope.votesUpdateTime = Date.now()
        $scope.votes.updateTime = Date.now()        
      });
    }

    $scope.deletevote = function(submission) {
      var url = '../api/post/' + submission._id + '/vote';
      $http.delete(url).success(function(data) {
        delete $scope.votes.map[submission._id]
        $scope.votes.updateTime = Date.now();
      })
    }
  }])


  .controller('SubmitCtrl', ['$scope', '$http','$location', 'tokenFactory', '$routeParams', '$rootScope', function($scope, $http, $location, tokenFactory, $routeParams, $rootScope) {
  	$scope.submitPost = function(submission) {
      var url = '../api/sub/' + $routeParams.subName + '/post'

  		var data = {'title':submission.title, 'comment':submission.comment};
  		$http.post(url,data).success(function(data, status) {
  			console.log(data);
  			$location.path('/post/' + data._id)
  		});
  	}
  }])


  .controller('PostCtrl', ['$scope', '$http','$routeParams', '$location', 'tokenFactory', 'postVoteFactory', '$rootScope', function($scope, $http, $routeParams, $location, tokenFactory, postVoteFactory, $rootScope) {
    $scope.votes = postVoteFactory.votes;
    $scope.posts = postVoteFactory.posts;
    $scope.tokenMap = tokenFactory.tokenMap;

  	$http.get('../post/' + $routeParams.postID).success(function(data,status,headers,config) {
  		$scope.post = data;
      $scope.postEdit.comment = data.comment;
  	}).
	 	error(function(data,status,headers,config) {

  	});

   $scope.editPost = function(postEdit) {
      $http.put('../post/' + $routeParams.postID, postEdit).success(function(data,status,headers,config) {
        $scope.post.comment = data.comment;
        $rootScope.$broadcast('edit-post');
      });
    };
 
    $scope.post_reply = function(comment) {
      $http.post('../api/post/' + $routeParams.postID + '/comment', {'username':tokenFactory.username, 'comment': comment, 'comment_parent': ''}).success(function(data,status) {
        $rootScope.$broadcast('reply-post')
      });
    }

    $scope.delete = function(post) {
      $http.delete('../post/' + post._id, {}).success(function(data,status) {
        $location.path('/#/');         
      })
    }

  	$scope.post = {};
    $scope.postEdit = {'comment':''};
  }])


  .controller('LoginCtrl', ['$scope', '$http', 'tokenFactory', function($scope,$http,tokenFactory) {
 		$scope.login_status = "";
 		$scope.logged_in = false;

 		$scope.show_registration = false;
 		$scope.show_login = false;

    if(tokenFactory.token != null && tokenFactory.username != null) {
      $scope.logged_in = true;
      $scope.show_login = false;
    }

 		$scope.login = function(credentials) {
 			$http.post('../api/login', {'username':credentials.username, 'password':credentials.password}).success(function (data, status) {
 				//tokenFactory.token = data.token;
 				//tokenFactory.username = credentials.username;

        tokenFactory.setLoginSession(data.token, credentials.username);
 				$scope.login_status = "login succeeded";
 				$scope.logged_in = true;
 				$scope.show_login = false;
 			})
 			.error(function(data,status) {
 				$scope.login_status = "login failed";
 			});
 		}

 		$scope.toggleRegistration = function() {
 			if($scope.show_registration)
 				$scope.show_registration = false;
 			else {
 				$scope.show_registration = true;
 				$scope.show_login = false;
 			}
 		}

 		$scope.toggleLogin = function() {
 			if($scope.show_login)
 				$scope.show_login = false;
 			else {
 				$scope.show_login = true;
 				$scope.show_registration = false;
 			}
 		}

 		$scope.register = function(credentials) {
 			$http.post('../api/register', {'username':credentials.username, 'password':credentials.password, 'email':credentials.email}).success(function(data,status) {
        tokenFactory.setLoginSession(data.token, credentials.username);
 				$scope.login_status = "login succeeded";
 				$scope.logged_in = true;
 				$scope.show_registration = false;
 			})
 			.error(function(data,status) {
 				$scope.login_status = "login failed";
 			});
 		}
  }])


  .controller('CommentCtrl', ['$scope', '$http', '$routeParams', 'tokenFactory', 'commentFactory', 'commentVoteFactory', '$rootScope', function($scope,$http,$routeParams,tokenFactory,commentFactory,commentVoteFactory, $rootScope){
    commentVoteFactory.getComments($routeParams.postID);
    $scope.tree = commentVoteFactory.comments.tree;
    $scope.comments = commentVoteFactory.comments;


    $scope.post_reply = function(parent, comment) {
      var parent_id = parent != '' || null ? parent._id : '';

      $http.post('../api/post/' + $routeParams.postID + '/comment', {'comment': comment, 'comment_parent': parent_id}).success(function(data,status) {
        $scope.comments.list.push(data);
        $scope.comments.tree = commentFactory.createCommentTree($scope.comments.list);

        if(parent == '') {
          $rootScope.$broadcast('reply-post');
        }else {
          $rootScope.$broadcast('reply-comment-'+parent._id);
        }
      });
    }

    $scope.post_reply_parent = function(comment) {
      $scope.post_reply('', comment)

    }

    $scope.edit = function(comment, new_edit) {
      $http.put('../api/post/' + $routeParams.postID + '/comment/' + comment._id, {'comment': new_edit}).success(function(data,status) {
        $rootScope.$broadcast('edit-comment-'+comment._id);
      })
    }

    $scope.delete = function(comment) {
      $http.delete('../api/post/' + $routeParams.postID + '/comment/' + comment._id, {}).success(function(data,status) {
        comment.username = '[deleted]';
        comment.comment = '[deleted]';
        $rootScope.$broadcast('delete-comment-'+comment._id);
      })
    }

    $scope.upvote = function(submission) {
      commentVoteFactory.upvote(submission);
    }

    $scope.downvote = function(submission) {
      commentVoteFactory.downvote(submission);
    }

  }])


  .controller('NewSubCtrl', ['$scope', '$http', '$routeParams', 'tokenFactory', 'commentFactory', 'commentVoteFactory', function($scope,$http,$routeParams,tokenFactory,commentFactory,commentVoteFactory){
    $scope.form = {
      'name': null
    }

    $scope.createSub = function() {
      $http.post('../api/sub', {name: $scope.form.name}).success(function(data,status) {
      })
    }
  }]);